__author__ = 'ozbolt'

from random import randint
from math import fabs as abs
from time import clock_gettime
import os

def randPoint():
    return tuple([randint(0, W-1), randint(0,H-1)])

def touching(p1, p2):
    dx = abs(p1[0]-p2[0])
    dy = abs(p1[1]-p2[1])
    return dx<2 and dy<2

N = 99
W = 30
H = 16

SP = randPoint()
spStr = "'" + str(SP[0]) + "," + str(SP[1]) + "'"

print(SP)

mines = []

for i in range(N):
    while(True):
        np = randPoint()
        if not touching(np, SP):
            if np not in mines:
                break
    mines.append(np)

mineStr = "'"
for m in mines:
    mineStr += str(m[0]) + "," + str(m[1]) + ";"
mineStr = mineStr[:-1] + "'"

tajm = hex(int(clock_gettime(0)*100)%0xFFFFFF)[2:]
logFile = "./logs/mine_" + tajm + "_log.log"
exportFile = "./logs/mine_" + tajm + "_exp.txt"

cmd = "python ./main.py -debug " + logFile + " -export " + exportFile + " -width " + str(W) + " -height " + str(H) + " -start_point " + spStr + " -mines " + mineStr
f=os.popen(cmd)
for i in f.readlines():
    print(i, end="")






