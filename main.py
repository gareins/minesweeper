__author__ = 'ozbolt'

import sys
import logging

SQUARE = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]


class MineBox:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.known = False
        self.mine = False
        self.empty = False
        self.value = 0

    def __repr__(self):
        return "(%2d,%2d)" % (self.x, self.y)

    def __str__(self):
        return repr(self)

    def export_str(self):
        if not self.known:
            return " "
        if self.mine:
            return "M"
        elif self.empty:
            return "-"
        else:
            return str(self.value)

#noinspection PyAttributeOutsideInit
class Minesweeper:
    def __init__(self):
        self.mines = None
        self.matrix = None
        self.export = False

    def set_width(self, value):
        value = float(value)
        if int(value) != value:
            raise ValueError("Not an integer: %f" % value)
        elif not 0 < value:
            raise ValueError("Out of Bounds: %d below 0" % value)
        else:
            self.width = int(value)

    def set_height(self, value):
        value = float(value)
        if int(value) != value:
            raise ValueError("Not an integer: %f" % value)
        elif not 0 < value:
            raise ValueError("Out of Bounds: %d below 0" % value)
        else:
            self.height = int(value)

    def set_start_point(self, beginAt):
        beginAt = beginAt.split(",")
        self.start_point = tuple([int(beginAt[0]), int(beginAt[1])])

    def __str__(self):
        row = '+---' * (self.width) + '+\n'
        toWrite = ""
        for x in range(self.width):
            toWrite += "  %02d" % x

        toWrite += "\n" + row
        for x in range(self.height):
            for y in range(self.width):
                toWrite += "| " + self.matrix[x][y].export_str() + " "
            toWrite += "| %d\n%s" % (x, row)
        return toWrite

    def export_txt(self):
        with open(self.export_file, "a") as fp:
            fp.write(str(self))

    def set_export_file(self, fn="./mines_export.txt"):
        self.export = True
        self.export_file = fn

    def clean_up(self):
        if (self.export):
            self.export_txt()
            logging.debug("Exported: " + self.export_file)

    def switch_debug_mode(self, fn="mines_log.log"):
        logging.basicConfig(
            level=logging.DEBUG,
            filename=fn,
        )

    def import_mines(self, entry):
        self.mines = []
        for double in entry.split(";"):
            double = double.split(",")
            pair = [int(double[0]), int(double[1])]
            self.mines.append(tuple(pair))

        self.mines_ready = len(self.mines) > 0

    def parseArguments(self):
        #arguments, that need a pair
        pair_args = {
            "mines": self.import_mines,
            "width": self.set_width,
            "height": self.set_height,
            "start_point": self.set_start_point
        }
        #arguments with optional pair
        opt_pair_args = {
            "export": self.set_export_file,
            "debug": self.switch_debug_mode
        }
        #argument, without a pair
        no_pair_args = {
        }

        for i, arg in enumerate(sys.argv[1:]):
            if arg.startswith("-"):
                arg = arg[1:]
                if arg in no_pair_args.keys():
                    no_pair_args[arg]()

                elif arg in opt_pair_args.keys():
                    try:
                        if not sys.argv[i + 2].startswith("-"):
                            opt_pair_args[arg](sys.argv[i + 2])
                        else:
                            opt_pair_args[arg]()
                    except ValueError:
                        logging.exception("bad opt. option: -" + arg + " " + sys.argv[i + 2])
                    except IndexError:
                        opt_pair_args[arg]()
                        pass

                elif arg in pair_args.keys():
                    try:
                        pair_args[arg](sys.argv[i + 2])
                    except ValueError as e:
                        logging.exception("bad option: -" + arg + " " + sys.argv[i + 2])
                        exit(1)
                else:
                    logging.exception("bad option: " + arg)
                    exit(1)
            else:
                continue

    def check_and_prepare(self):
        obligatory = ["height", "width", "start_point", "mines_ready"]
        for o in obligatory:
            if o not in self.__dict__:
                raise ValueError("Missing: " + o)

        self.matrix = [[MineBox(x, y) for x in range(self.width)] for y in range(self.height)]

        for m in self.mines:
            box = self.matrix[m[1]][m[0]]
            box.mine = True
            for s in self.get_square(box):
                s.value += 1

        for x in range(self.width):
            for y in range(self.height):
                if self.matrix[y][x].value == 0:
                    self.matrix[y][x].empty = not self.matrix[y][x].mine



    ### Actual Solver ###

    #  implement:
    # 1 - open_initial
    #   -- get active
    #   -- get done
    #   -- to_check = active
    # 2 - take from to_check
    #   -- simple_check
    #   -- if solvable:
    #   --- flag done
    #   --- if new one not mine -> add to_check
    #   --- check all surrounding
    #   ---- if active -> add to_check
    #   ---- else leave
    #   -- else remove from to_check ->2
    # 3 - if to_check empty:
    #   -- advanced solver

    def solve(self):
        toOpen = self.matrix[self.start_point[1]][self.start_point[0]]
        toOpen.known = True

        self.active = {}
        self.done = []
        self.to_check = []

        self.open_square(toOpen)
        self.export_txt()

        #Iteration, iteration
        while (True):
            self.log_active()
            self.check_checks()

            self.log_boxes(self.done, "Done: ")
            self.log_boxes(self.to_check, "To_check: ")

            isNew = self.solve_simple()
            if isNew:
                self.export_txt()
            else:
                break

    def check_checks(self):
        for box in self.to_check:

            if box in self.done or box.mine:
                continue

            unknowns = []
            num_mines = box.value

            # if one of the boxes around center is not yet known, it's added to unknowns.
            # if one of those boxes is a mine, then we decrease
            # the number of mines, to be filled into unknowns

            for s_box in self.get_square(box):
                if not s_box.known:
                    unknowns.append(s_box)
                elif s_box.mine:
                    num_mines -= 1

            if len(unknowns) > 0:
                self.active[box] = (num_mines, tuple(unknowns))
            else:
                self.active.pop(box, None)
                self.done.append(box)

        self.to_check = []

    def solve_simple(self):
        toSolve = {}

        for box, (num_mines, unknowns) in self.active.items():
            if num_mines == 0:
                toSolve[unknowns] = "-"
            elif num_mines == len(unknowns):
                toSolve[unknowns] = "m"

        logging.debug(toSolve)
        self.manage_solved(toSolve)
        return len(toSolve) > 0


    def manage_solved(self, to_solve):
        for boxs, fill in to_solve.items():
            for box in boxs:
                #Sanity check
                if (box.mine and fill == "-") or (not box.mine and fill == "m"):
                    raise Exception("narobe resevanje!")

                box.known = True
                if box.empty:
                    self.open_square(box)

                for s_box in self.get_square(box):
                    if s_box.known:
                        self.to_check.append(s_box)
                        #logging.debug(str(box) + " -> " + str(s_box))

    def get_square(self, box):
        boxes = []
        x = box.x
        y = box.y

        for s in SQUARE:
            if (0 <= y + s[1] < self.height) and (0 <= x + s[0] < self.width):
                boxes.append(self.matrix[y + s[1]][x + s[0]])
        return boxes

    def open_square(self, box):
        for s in self.get_square(box):
            #Stop recursion with if s.known
            if not s.known:
                s.known = True
                #If empty -> recursion with another square
                if s.empty:
                    self.open_square(s)
                #Else -> just add it to actives
                else:
                    self.to_check.append(s)



    def log_boxes(self, to_log, comment=""):
        toLog = ["\n\t\t(%2d:%2d) -> %d" % (a.x, a.y, a.value) for a in to_log]
        logging.debug(comment + "".join(toLog))

    def log_active(self):
        toLog = ""
        for box, (num_mines, unknowns) in self.active.items():
            empties = "U[" + "".join(["(%d,%d)" % (el.x, el.y) for el in unknowns]) + "]"
            toLog += "\n\t\t[%2d,%2d]\t %d\t %s" % (num_mines, box.x, box.y, empties)

        logging.debug(" Active: " + "".join(toLog))


    ### Runner ###
    def run(self):
        self.parseArguments()
        self.check_and_prepare()
        self.solve()
        self.clean_up()


if __name__ == "__main__":
    Minesweeper().run();



